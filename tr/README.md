Otağa Hoşgeldiniz!

![Otağ Logo](https://ilgilenio.github.io/Otag/img/otag.svg)


Otağ'a girerken
-----
```javascript
var Esen='Esen'.set('Esenlikler ad₺!',{ad:'Yertinç'})
Esen.val // {ad:"Yertinç"};

O.ready.then(b=>b.append(Esen)); //Sayfaya Ekle
```

```html
<div class="Esen">Esenlikler Yertinç!</div>
```

```javascript
Esen.val= {ad:"Otağ'a"}
```

```html
<div class="Esen">Esenlikler Otağ'a!</div>
```
- - - -

Örneği anlamak için :

| | | | | | | |
|-|-|-|-|-|-|-|
| [[Öge]] | [Öge oluşturma](Yazı.init) | [[.set]] | [[.val]] | [[.append]] | [[O]] | [[O.ready]] |

Örnek tasarılar:
-----
1. [📙 **Atasözleri**](https://github.com/ilgilenio/ilgilenio.github.io/blob/master/Otag/ornekler/Atasozleri/README.md) Bet(Sayfa) Omurgasını ve temel Otağ yöntemlerini anlamak için kapsamlı örnek.
2. [👤 **CodePen Görüntüleyici**](https://codepen.io/otag/pen/jxGyjr) CodePen üzerinde CodePen Profil Görüntüleyici.
3. 👉🏼 Yaptığınız tasarıyı sergilemek için yollayın.