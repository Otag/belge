# Otağ Ana Sınıfı

Otağ'da bulunan alt sınıflar

| Yöntem                  | Açıklama |                           
| -----------------       | --------------------------------------|
| [Page](./Page.MD)       | Sayfa omurgası/Geçmiş Yöneticisi. |
| [Chain](./Chain.MD)     | İşlevleri sırayla işler.     |
| [Time](./Time.MD)       | Zaman Nesnesi |
| [i18n](./i18n.MD)       | Uluslararasılaştırma sınıfı.   |
| [Disk](./Disk.MD)       | Yerel yığınak nesnesi.  |
| [Tor](./Tor.MD)         | Ağ işlevleri sınıfı     |
| [O.ready](./O.ready.MD) | Belge yüklenme Sözü     |