## .stop

Bu [İşlev](./İşlev.MD) uzantısı, çakılan etkinliğin Öge ağacındaki üst ögelere yayılmasını engellemek amacıyla kullanılır.

**Örnek:**
```javascript
  'a.Düğme'.kur({
    click:(()=>{
      console.log()
    }).stop
  })
```

Bu, şu anlama gelir: Eğer düğmeye tıklanmışsa, üst ögelere de tıklanma bilgisi gönderme.