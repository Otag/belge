'virgülle,ayrılmış,açarlar'**.from(** *Nesne{ }* **)**

**.from** bir **[Yazı](./Yazı.MD)** uzantısıdır.
İstenilen anahtarların iyeliğindeki değerleri Nesne içinden bularak bir `Dizi` oluşturur. Girilen sıra önemlidir.

```javascript
'gün,ay,yıl'.from({
    gün: 30,
    yıl: 1922,
    ay: 8
})
//Dizi [30, 8, 1922]
```

JavaScript dilinde **[Öge](./Öge.MD)** de bir `Nesne` olduğu için **[Öge](./Öge.MD)**nin özelliklerini de getirebilirsiniz

```javascript
Öge='.Bir#Öge'.el //Bir Öge oluştur

'id,className'.from(Öge)
//Dizi ['Öge', 'Bir']
```