# Yazı Nedir

Yazı(`String`), bir veya daha fazla imin ardarda eklenmesinden oluşan bir veri türüdür.

Otağ'ın tanımladığı [Tamga](./Tamga.MD) da bir `Yazı`dır.

Otağ'da Yazı üserinde belirli uzantılar oluşturulmuştur.

## Yazı Uzantıları

| Yöntem        | Açıklama |                           
| ------------- | ----------------------------------------------------|
| [.vars()](./Yazı.vars.MD) | Değişkenli Yazıyı işler.                  |
| [.from()](./Yazı.from.MD) | Nesneden Dizi oluşturur.                  |
| [.kur()](./Yazı.kur.MD)   | Yeni bir Sanal Otağ kurar .               |
| [.get](./Yazı.get.MD)     | Sayfada bulunan [Öge](./Öge.MD)leri bulur.|
| [.of()](./Yazı.of.MD)     | Verilen `Nesne`deki ilgili açarları toplar|


> Yazı'ya benzeyen [Tamga uzantıları](./Tamga.MD)nı da inceleyin