# İşlev Uzantıları

JavaScript dilinde işlevler için bulunan **.bind** **.call** gibi yöntemlere ek olarak Otağ'da şu yöntem ve geliştirmeler vardır.

| Yöntem | Açıklama |                           
|-|-|
| [.debounce()](./İşlev.debounce.MD) | İşlevi gecikmeli işleve dönüştürür |
| [.prom()](./İşlev.prom.MD)         | İşlevi Söze dönüştürür             |
| [.prevent](./İşlev.prevent.MD)     | İşleve preventDefault() atar       |
| [.stop](./İşlev.stop.MD)           | İşleve stopPropagation() atar      |
