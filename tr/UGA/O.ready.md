**O.ready => **Söz .then(**{body, head}**)

Bir belge yüklenene kadar içine [Öge](./Öge.MD) koyamayız. Bunun için **DOMContentLoaded** etkinliğinin çakılması gerekir.

```javascript
// Yalın JavaScript ile yazılmış
let Öge=document.createElement('div'); 		//Belge yüklenmeden oluşturuldu
Öge.className='Öge';


document.addEventListener('DOMContentLoaded',function(){
	document.body.appendChild(Öge); 	//Belge yüklenince eklendi.
});
```

Bu yazışın uzun olması nedeniyle O.ready `Söz`ü vardır. 
Belge yüklendiğinde `Söz` çözümlenir. 


```javascript
//Otağ.JS ile yazılmış
let Öge='Öge'.el;

O.ready.then(({body})=>body.append(Öge));
```

