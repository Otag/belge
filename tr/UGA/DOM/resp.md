**.resp(** {Özellik:Tepki} _Nesne_ **)**

**.resp(** Özellik _Yazı_ , Tepki _İşlev_ **)**

----
Bir [[Öge]]'ye değişime duyarlı bir özellik tanımayabilirsiniz.

**.resp** yöntemi [[Öge]]'ye, özelliğin önceki ve yeni değerine ulaşabileceğiniz bir işlev tanımlamanızı sağlar.

[[Öge]]'ye **this**; **Özellik**'in önceki değerine **this._Özellik_** yazarak ulaşabilirsiniz. **Özellik**'in yeni atanan değeri ise tanımlayacağınız işlevin ilk girdisi olarak gelecektir.

```javascript
let Sayı='Sayı'
	.prop({değeri:5})
	.resp('değeri',function(yeniDeğer){
		console.log('Değişim:',yeniDeğer-this.değeri);
	})
Sayı.değeri=15; // Değişim: 10

```

#### Uyarı:
Eğer atanan değerde değişim yok ise işleviniz çağrılmayacaktır. 
