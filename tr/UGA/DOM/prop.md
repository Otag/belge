**.prop(** {Özellik:Değer} _Nesne_ **)**

**.prop(** Özellik _Yazı_, Değer _HerhangibirŞey_ **)**

Bir Öge'ye özellik/ler tanımlar.

2 tür kullanım vardır : Tekil ve Çoğul
Eğer tek bir özellik tanımlayacaksanız **Tekil Kullanım**ı;
birden çok özellik tanımlayacaksanız **Çoğul Kullanım**ı tercih ediniz.


### Kullanım

Aşağıdaki örnek belgeye tıklanınca kaybolan bir Öge ekler.
```javascript
//Çoğul kullanım (Girdi olarak Nesne verilir)
var Öge='Öge'.prop({
	onclick:function(){
		this.disp(0);
	}
});

//Tekil kullanım (Girdi olarak 1:Özellik Adı 2:Özellik Değeri)
var Öge='Öge'.prop('onclick',function(){
	this.disp(0);
});

O.ready.then(b=>b.append(Öge));	//Sayfaya ekle
```


---

```javascript
var Öge='Öge'.prop({
	a:1,
	b:2
});
Öge.a+Öge.b //3

O.ready.then(b=>b.append(Öge));	//Sayfaya ekle
```
