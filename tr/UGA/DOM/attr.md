**.attr(** {Nitelik:Değer} _Nesne_ **)**

**.attr(** Nitelik _Yazı_, Değer _HerhangibirŞey_ **)**

Bir Öge'ye nitelik/ler tanımlar.

2 tür kullanım vardır : Tekli ve Çoklu
Eğer tek bir nitelik tanımlayacaksanız **Tekli Kullanım**ı;
birden çok nitelik tanımlayacaksanız **Çoklu Kullanım**ı tercih ediniz.


### Kullanım

```javascript
//Çoklu kullanım (Girdi olarak Nesne verilir)
var Öge='input'.attr({
	type:'button'
});

//Tekli kullanım (Girdi olarak 1:Nitelik Adı 2:Nitelik Değeri)
var Öge='input'.attr('type','button');

O.ready.then(b=>b.append(Öge));
```

Bu örnek belgeye Düğme türünde bir Giriş Ögesi ekler.



### [[.prop]] yönteminden farkı
**[[.prop]]** yönteminden farkı HTML ve CSS yorumlayıcısında gözükmesidir.


```html
<input type="button"/>
```
Yukarıdaki örnekte attr kullandığımız için HTML yorumlayıcısında _type="button"_ deyişini gördük.
Bu bize CSS yazarken ve sayfadaki ilgili ögeleri getirirken yardım eder.

```css
input[type="button"]{
	padding:3px 7px;
}
```


```javascript
'input[type="button"]'.get() // [input]
```

_Örnekteki yazımın açıklaması için bkz:_ [[Yazı.get]]

