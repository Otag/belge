**.set(** _Yazı_ **)**

**.set** ön tanımlı yöntemi bir Öge’nin içine Yazı yazar

```javascript
let Öge=''.set("Otağ");

O.ready.then(b=>b.append(Öge)); //Sayfaya ekle
```

```html
<div>Otağ</div>
```
Ancak çok dilli sistem tasarlıyorsanız [[O.i18n]] belgelendirmesinde anlatılan gelişmiş kullanıma bakmanızda fayda vardır.
