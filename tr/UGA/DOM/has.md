**.has(** ÖgeBağları _Nesne_ **)**
``` javascript
let Bağ={
    ad:'a.ad'.init(), //Öge <a class="ad"></a>
    soy:'a.soy'.init() //Öge <a class="soy"></a>
}
```
Bağ nesnesi değer olarak Ögeleri içeren açarlardan oluşan, _bildiğimiz JavaScript Nesnesi_ 'dir.

**.has** yöntemi bağ nesnesinde tanımlanan nesneler ile yöntemin uygulandığı nesne arasında ilişki kurar.
İlişki kurmadan eklemek için **[[.layout]]** yöntemini tercih edin.

``` javascript
let Ata='Ata'.has({
    ufak:'Çocuk'.init(),
    iri:'Çocuk'.init()
});
O.ready.then(b=>b.append(Ata)); //Sayfaya ekle
```
Yukarıda yapılan işlem aşağıdaki HTML çıktısını üretir
``` html
<div class="Ata">
    <div class="Çocuk"></div>
    <div class="Çocuk"></div>
</div>
```

### Erişim
JavaScript dilinde bulunan **.children** ( *Ata.children* ) işlevi sayfa dışında çalışmadığı için kullanışsızdır.
**.has** yöntemiyle ise; Ata Ögesi sayfada bulunmasa bile Çocuklara erişilebilir.

``` javascript
let Ata='Ata'.has({
    ufak:'#ufak.Çocuk'.init(),
    iri:'#iri.Çocuk'.init()
});

Ata.View.ufak //<div id="ufak" class="Çocuk"></div>  // sayfaya eklemeden erişebiliyoruz
Ata.View.ufak.parent //<div class="Ata"></div>       // çocuktan Ata'ya da bağ var
Ata.children[0]                                      // X ÇALIŞMAZ X
Ata.View.ufak.parentElement                          // X ÇALIŞMAZ X

O.ready.then(b=>b.append(Ata)); //Sayfaya ekle
```
### Erişim İşlevi (.V)
Ögenin alt ögeleri arttıkça ve nesne derinleştikçe erişim zorlaşacağı daha kısa bir yazım amacıyla 
.V yöntemi tasarlanmıştır.

``` javascript
let UralAltay='DilAilesi'.has({
    Altay:'Altay Kolu'.has({
        Türk:'Türk Dilleri'.has({
            Oğuz:'Oğuz Takımı'.has({
                Türkiye:'Türkiye Türkçesi Dili'
            }),
        }),
        Moğol:'Moğol Dilleri',
        Tunguz:'Tunguz Dilleri'
    })
    Ural:'Ural Kolu'.has({
        Fin:'Fin Dili',
        Eston:'Estonca Dili',
        Macar:'Macarca Dili'
    })
});
UralAltay.View.Altay.View.Türk.View.Oğuz.View.Türkiye;  
// <div class="Türiye Türkçesi Dili"></div>

UralAltay.V('Altay:Türk:Oğuz:Türkiye');                 
// <div class="Türiye Türkçesi Dili"></div>

O.ready.then(b=>b.append(UralAltay)); //Sayfaya ekle
```

### Kısa Yazımlı BağNesnesi
Bağ nesnesinde sadece öge oluşturuyor ve üzerinde işlem yapmıyorsanız ( *.prop vs. gibi* ) .init yazmanıza gerek yoktur. Yöntem; verdiğiniz Yazı değeriyle Öge oluşturur.
``` javascript
let Ata='Ata'.has({
    ufak:'Çocuk',       // .init() yazmamıza gerek yok
    iri:'Çocuk'         //
});
O.ready.then(b=>b.append(Ata)); //Sayfaya ekle
```

### Bir ögenin veri karşılığı ( .val )
Otağ Felsefesinde Ögeler Verilerin görsel Modelleri olduklarından belirli verilere sahip olmaları, değer atandıkları zaman bu değere göre kendini güncellemesi beklenir.
Öge **.val** komutu ile **.has** ile bağlanan alt ögelerin değerine erişmeyi sağlar
```javascript
    let Özellik={
        onkeyup:function(){
            console.log(this.parent.val); // {ad:'..',soyad:'..'}
        }
    }
    
    let Düzenle='Düzenle'.has({
        ad:'input.ad'.prop(Özellik),
        soy:'input.soy'.prop(Özellik)
    });
    Düzenle.val                           //{ad:'',soyad:''}
    
    O.ready.then(b=>b.append(Düzenle));   //Sayfaya ekle

```
Örnekteki yiv; ad ve soy girişleri değiştikçe Düzenle Ögesinin değerini söylüyor.

#### Veri karşılığında gözükmeyi engelleme
Bir alt ögenin veri karşılığında gözükmesini engellemek için başına _ konulabilir.
``` javascript
let Düzenle='Düzenle'.has({
    ad:'input.ad'.prop(Özellik),
    soy:'input.soy'.prop(Özellik),
    _kayıt:'input[type="button"]'   // _ ile veride gözükmesini engelledik
            .set('Kaydet')              
});
Düzenle.View.kayıt                  // <input type="button"/>   // Öge erişilebilir
Düzenle.val                         // {ad:'',soyad:''}         // Veride gözükmedi

O.ready.then(b=>b.append(Düzenle));   //Sayfaya ekle
```