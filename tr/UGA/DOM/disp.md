**.disp(** Göster _Mantıksal_ **)**

Bir Öge'nin görünürlüğünü etkiler.

### Kullanım

Göster girdisi Doğru olursa Öge Gözükür.
Yanlış ya da Boş olursa Öge gözükmez.

---
Görünmez bir öge ekleyelim;
```javascript
let Öge=''.disp();

O.ready.then(b=>b.append(Öge));
```

```html
<div style="display: none"></div>
```

Öge gözüksün diyelim;
```javascript
Öge.disp(true);
```

```html
<div style="display: block"></div>
```
-----
Bu yöntemin en önemli özelliği önceki görünürlük durumunu korumasıdır.


```css
#esnekKutu{
	display: flex
}
```

```javascript
let Kutu='#esnekKutu'.disp(); 		        //Görünürlük : Yok

O.ready.then(b=>b.append(Kutu.disp(1)));	//Görünürlük : Var, Esnek ==> Sayfaya ekle
```

```html
<div id="esnekKutu" style="display: flex"></div> 	<!--Görünürlük durumu korundu-->
```