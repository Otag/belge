|![](https://ilgilenio.github.io/Otag/img/o.min.svg) | Otağ™ JS |
|--|--|
| [Otağ](./O.MD)    | [Öge](./Öge.MD)     |
| [Yazı](./Yazı.MD) | [İşlev](./İşlev.MD) |
-----
[📅 Çizelge](https://ilgilenio.github.io/Otag/cizelge/)

[💬 Tartışma](https://t.me/OtagJS)

[👍 Destekle](https://fb.me/OtagJS)

<a href="https://liberapay.com/otag/donate" ping="https://otagjs.org/s/bilgi?bagis">Bağış Yap: <img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>