## .debounce(ms)

Bu [İşlev](./İşlev.MD) uzantısı, işlev çağrılarını _sınırlandırmak_, _gecikmeli_ `İşlev` tanımlamak amaçlarıyla kullanılır.
_Bağlam korunur._

**Örnek:**
```javascript
debounceLog = console.log.debounce(3000)
debounceLog('Armut') //3s sonra Armut yazar
```
Eğer bu örnekte 3s içinde/arka arkaya birden fazla debounceLog çağırırsanız sadece son çağrınız çalıştırılır.

- - - -
**Örnek 2**
```javascript
document.addEventListener("scroll", (() => {
  console.log('çalıştı ',+new Date)
}).debounce(100));
```

Bu örnek ise **.debounce** yönteminin en yaygın kullanıldığı örnektir. 
Çok fazla çağrılan işlevlerin belirli bir süre için sınırlandırılmasına olanak vererek sık yineli ve yüksek iş gücü gerektiren işlevlerin
verimi artırır, taşınabilirlerin pilini korur.