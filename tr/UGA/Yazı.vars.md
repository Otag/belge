'Değişken₺ Barındıran Yazı' **.vars(** *Nesne{Değişken:'Yazı' }* **)**

**DeğişkenAdı₺** şeklinde tanımlanan değişken barındıran  [Yazı](./Yazı.MD)'yı işler.

**₺,₸,₼,$** imleri değişken tanımlamada kullanılabilir.
 

```javascript
let Söz = "Otağ'a hoşgeldin ad₺ soy₸".vars({
    ad: 'Kapgan',
    soy: 'Kağan'
});

```

Söz: Otağ'a hoşgeldin Kapgan Kağan