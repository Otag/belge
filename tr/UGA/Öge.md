Öge Yöntemleri

Öge bir Web uygulamasının Görünüm katmanını oluşturur. Bu yüzden Otağ geliştirilirken Öge'ye önem verilmiş ve kullanışlı yöntemler tasarlamak amaçlanmıştır.

| Yöntem        | Açıklama |                           
| ------------- | --------------------------------------|
| [[.set]]	| Öge içine yazar. 			|
| [[.val]]	| Öge'nin değerini verir. 		|
| [[.append]]	| Öge'nin içine Öge/ler ekler. 		|
| [[.has]]	| Öge'ye Öge bağlar.			|
| [[.layout]]	| Öge içinde şablon oluşturur. 		|
| [[.attr]]	| Öge'ye nitelik/ler ekler. 		|
| [[.prop]]	| Öge'ye özellik/ler ekler. 		|
| [[.resp]]	| Öge'ye duyarlı değişken/özellik ekler.|
| [[.disp]]	| Öge'nin görünürlüğünü değiştirir. 	|
| [[.destroy]]	| Öge'yi gecikmeli kaldırır.		|
| [[.interval]]	| Öge'ye zaman aralıklı işlev tanımlar	|
| [[.Class]]	| Öge'ye sınıf/lar ekler/kaldırır	|

Bu yöntemlerin yanı sıra Öge oluşturmak ve değişiklik yapmak en sık yapılan iş olduğu için yazım yalınlaştırılmış, kesintisiz yazımlara olanak sağlamak için bağlamlar korunmuştur (_Yöntemler çağrıldığı zaman Öge'nin kendisi döner_).

Bir Otağ uygulamasında DOM (Belge Nesne Kipi) Ögesi [[Yazı]] ile [doğrudan](Yazı.init) ya da yöntemlerin içinde ([[.has]],[[.layout]]) dolaylı olarak oluşturulabilir. Öge'nin sunulan değerine **[[.val]]** ile erişilebilir
