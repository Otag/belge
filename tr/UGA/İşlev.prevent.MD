## .prevent

Bu [İşlev](./İşlev.MD) uzantısı, çakılan etkinliğin ön tanımlı işini engellemek amacıyla kullanılır.

**Örnek:**
```javascript
  'a.Düğme[href="yol.html"]'.kur({
    click:(()=>{
      alert("Ebliğ toygursa közi yolka bolur") 
      // Ev iyesi doyurunca, konuğun gözü yolda olur
    }).prevent
  })
```

Bu, şu anlama gelir: Gerçekte düğmeye tıklandığında, tarayıcı `yol.html` bulunağına gidecekti. Ancak prevent ile bu engellendi