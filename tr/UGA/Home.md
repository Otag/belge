Otağa Hoşgeldiniz!

![Otağ Logo](https://ilgilenio.github.io/Otag/img/otag.svg)

Otağ'a girerken
-----
```javascript
import O from 'otag'

'h1.Esen:Esenlikler Yertinç'.to = 'body'
```

```html
<body>
  <h1 class="Esen"> Esenlikler Yertinç! </h1>
</body>
```

[Otağ Sözdizimi Güzellikleri](./Sözdizimi.MD)ni incele

## Otağ Nedir?

[Otağ](https://otagjs.org) JS; yalın, bütünleşik, sözdizimi güzel yapısıyla hızlıca, küçük boyutlu ağ uygulamaları yazabilmenizi sağlayan bir JavaScript çatısıdır.

[Otağ'ın sağladığı üstünlükler](./Neden.MD)i incele.

Otağ gerçekte bir ön uç çatısıdır, ancak JavaScript'in yapısı onun bazı sınıflarını arka uçta da kullanmaya olanak verir.

[Otağ Node Bileşeni](./NodeJS.MD)ni gör.

## Sınıf(kütüphane) mi yoksa Çatı mı ?

Otağ 1.0'dan beri sıkça duyduğumuz bu soruyu yanıtlamadan önce kütüphane ve çatı kavramlarını tanımlıyoruz.

**Kütüphane** : Kütüphane ya da sınıf, belirli bir konuda işlev ve yöntemlerin toplandığı yapıdır. Kendisi gibi başka sınıfları da içerebilir. 

ÖR: Matematik sınıfı içinde Ayrık Matematik alt sınıfı olması

**Çatı** : Belirli bir amaç için özelleşmiş, genellikle yazıldığı dilin üzerine bir katman olarak kurulur, yiv bütünlüğünü ve düzenini sağlayan belirli **yapı**lar sunar

Otağ, JavaScript'in olanaklarını _etkin olarak_ kullanır ve sunduğu desenler ile ağ uygulaması yapmayı kolaylaştırma amacı güder. Bu özellikleri ile bir çatıdır diyebiliriz.

Bunun yanında

```javascript
import {araç} from 'otag'
```
biçiminde kullanabileceğiniz araçlar(sınıflar) içerir.

## Araçlar

* [Disk](https://belge.otagjs.org/tr/UGA/Disk.html) (ES6 Proxy) Yerel Yığınak soyutlayıcısı (localStorage abstractor)
* [Page](https://belge.otagjs.org/tr/UGA/Page.html) Betler arası yönlendirme işletmeni
* [Time](https://belge.otagjs.org/tr/UGA/Time.html) Yalın zaman kütüphanesi
* [Tor](https://belge.otagjs.org/tr/UGA/Tor.html) Tor istemcisi (Network requester)
* [Chain](https://belge.otagjs.org/tr/UGA/Chain.html) Ardışıl işlevlerden Süreç derleyicisi

