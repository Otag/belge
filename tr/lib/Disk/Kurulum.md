## Kurulum / Kullanım

```bash
    npm i o.disk -g
```

#### ES6 Bileşeni olarak kullanma

```javascript   
    import Disk from 'o.disk'
```

#### NodeJS Bileşeni olarak kullanma

```javascript   
    const Disk = require('o.disk')
```

> Node Bileşeni olarak kullandığınızda verileri .odisk/ dizinine yazar

#### Geleneksel kullanım biçimi

HTML belgenize ekleyin

```html
<script src="https://cdn.jsdelivr.net/npm/o.disk@2.0.3/index.min.js"></script>
```
