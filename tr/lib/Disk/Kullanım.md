
UGA(Uygulama Geliştirme Arabirimi, API) hem ön uçta(tarayıcı) hem de arka uçta(NodeJS) aynı biçimdedir.


## Yığınağa yaz 
```javascript
Disk.açar = 'değer'
```

### Yığınağa bir Nesne yaz
```javascript
Disk.nesne = {
    açar: 'değer'
}
```

## Yığınaktan oku
```javascript
console.log(Disk.açar) // değer

console.log(Disk.nesne.açar) // değer
```

## Yığınaktan sil
```javascript
delete Disk.açar
console.log(Disk.açar) // null
```

#### Yığınaktan topluca sil
```javascript
Disk.rem(['açar', 'nesne'])
```

#### Yazılan açarı geçici olarak tut
```javascript
Disk.expire('açar', 20) // 20s sonra silinecek
```