# O.Disk

O.Disk, yerel yığınak (**localStorage**) üzerine kurulmuş bir soyutlamadır. Her türlü veriyi yazabilmenize olanak tanır. Uzun yazımlardan kurtarır. Yalın ve anlaşılabilir bir arayüz sunar.


```javascript
O.Disk.açar={
    a: 1
}
// localStorage.setItem(JSON.stringify({a: 1}))
```


**ES6 Bileşeni** olarak da kullanabileceğiniz **O.Disk**'i arka uçta da kullanabilirsiniz.

**Node Bileşeni** olarak kullandığınızda uygulamanızın çalıştığı dizine .odisk/ dizini kurar ve verileri buraya kaydeder.

### Yeterge

MIT Özgür Yazılım Yetergesi © OtagJS.org 