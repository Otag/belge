.toArray( _Nesne_ )

**Nesne** ya da **işlev girdileri(arguments)**'ni **Dizi**'ye dönüştürür.

``` javascript
let dizelge = Nesne.toArray({a: 1,b: 2,c: 3})

console.log(dizelge) // [1, 2, 3]
```