.map( **nsn** _Nesne_, **pusula** _Nesne_, **sınırla** _Mantıksal_, **öntanımlı** )

**Nesne** açarlarını belirtiğiniz şekilde değiştirir.
Aşağıdaki örnekte a'nın değerini b'den al şeklinde bir değişim yapılmıştır. 

``` javascript
let nsn   = {a: 5, b: 6, c: 16, d: 18}
,  pusula = {a: 'b',c: 'a', b: 'c'}
,  sonuç  = Nesne.map(nsn, pusula)
    
console.log(sonuç) // {a: 6, b: 16, c: 5, d: 18}
```

> !! Buradaki sonuç **Nesne**si YENİ OLUŞTURULMUŞ bir **Nesne**dir

##### Açar kazanımını sınırlama

Bu yeni oluşturulan **Nesne**yi dilerseniz sınırlayanilirsiniz. öntanımlı değerler ile bütün **Nesne**yi emmek yerine sadece _pusula_da belirttiğiniz açarlar emilmiş olur

``` javascript
let nsn   = {a: 5, b: 6, c: 16, d: 18}
,  pusula = {a: 'b',c: 'a', b: 'c'}
,  sonuç  = Nesne.map(nsn, pusula, true)
    
console.log(sonuç) // {a: 6, b: 16, c: 5} // d emilmedi
```

##### Ön tanımlı değeri kullanma

_nsn_'de bulunmayan açarlar için öntanımlı bir değer tanımlayabilirsiniz. 

``` javascript
let nsn    = {a: 5, b: 6, c: 16, d: 18}
,   pusula = {a: 'b',c: 'a', b: 'c',e: 'f'}
,   sonuç  = Nesne.map(nsn, pusula, true, 25)
    
console.log(sonuç) // {a: 6, b: 16, c: 5, e:25} 
// nsn.f yok, bu durumda ön tanımlı değer e'ye verilmiş oldu
```

