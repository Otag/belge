.match( **nsn** _Nesne_, **nsn2** _Nesne_, **alanlar** _Dizi_, **öntanımlı* )

_nsn2_ **Nesne**sindeki ilgili _alanlar_'ı _nsn_ içine taşır, eğer o açar _nsn_ içinde yoksa _öntanımlı_ değer verilir

``` javascript
let nsn   = {}
,   nsn2  = {a: 5, c: 6, d: 16}
,   sonuç = Nesne.match(nsn, nsn2, ['a', 'd', 'e'], 12)

console.log(sonuç) // {a: 5, d: 16, e :2} // e öntanımlı değer ile
```

> Buradaki _sonuç_ **Nesne**si aslında _nsn_'nin kendisidir.

##### Öntanımlı değer kullanmanın başka bir biçimi

_öntanımlı_ değerleri bir ilgili açarlar için karşılığı bulunan bir **Nesne** olarak verin. _ _def _ açarını da diğer bütün açarlar için kullanın

``` javascript
let nsn   = {}
,   nsn2  = {a: 5, c: 6, d: 16}
,   sonuç = Nesne.match(
        nsn, nsn2,
        ['a', 'd', 'e', 'f', 'g', 'h'], {e: 12, _def:10})

console.log(sonuç) // {a: 5, d: 16, e :2, f: 10, g: 10, h:10}
```