.cumul( _Nesne_, _Nesne_, ...  )

**Nesne**lerin değerlerini birleştirir 


``` javascript
let nsn   = {a: 8, b: 2, c: 3, d: -2}
,   nsn2  = {c: 4, d: 1, e: 2}
,   sonuç = Nesne.cumul(nsn, nsn2)

console.log(sonuç) // {a: 8, b: 2, c: 7, d: -1, e :2} // değer kaybı yok
```

> Buradaki _sonuç_ **Nesne**si aslında _nsn_'nin kendisidir.
