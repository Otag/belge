# Nesne

Nesne bir **Nesne** işleme kütüphanesidir. lodash ile benzerlik gösteren işlevler içerir.

Benzerlerinden ayrı olarak, yöntemlerin genelinde yeni **Nesne** oluşturmak yerine ilk girdi olarak verilen **Nesne** üzerine işler

 [Otag Topluluğu](https://otagjs.org) tarafından yaratılmıştır

### Yeterge

MIT Özgür Yazılım Yetergesi © OtagJS.org 
