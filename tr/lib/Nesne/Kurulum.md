## Kurulum / Kullanım

```bash
    npm i nesne -g
```

```javascript   
    import Nesne from 'nesne'
```

#### Geleneksel kullanım biçimi

```html
    <script src="https://cdn.jsdelivr.net/npm/nesne@1.0.1/Nesne.min.js"></script>
```