# Dene

<img src="https://media.giphy.com/media/cdI9qrqmXPTtLKu5bQ/giphy.gif" width="300px">

Dene bir birim sınama çatısıdır. Kütüphane yöntemlerini sınamak için birebirdir.

## Kurulum

```bash
npm i dene --save-dev
```

![](https://nodei.co/npm/dene.png)

### Yeterge

MIT Özgür Yazılım Yetergesi © OtagJS.org 