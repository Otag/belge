## Kullanım

Her bir yöntem/olay için sınama yazın

```javascript
// dene.js

let Dene=require('dene')

let Denemeler = {
  yalınSınama: () => return true,
  asyncSınama: () => {
    return new Promise((yerineGetir, boz) => {

      setTimeout(yerineGetir,1500) // 1.5s sonra başarılı bir sonuç verir

    }
  }
}

Dene(Denemeler)
```

Sınama betiğinizi package.json dosyasındaki scripts:test kısmına ekleyin

```json
  "scripts" : {
    "test"  : "node dene.js"
  }

```

Komut satırından sınamayı çağırın

```bash
npm test
```