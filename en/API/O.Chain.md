**O.Chain(** İşlevler _Dizi_ **)** => _İşlev_( **(**ilkGirdi**)** =>_Söz_ )

İki aşamada kullanılır
###### İlk aşama
Çağırıldığı zaman Dizi olarak tanımlanan işlevleri sırayla işleyecek bir İşlev döndürür.

```javascript
 var Zincir=O.Chain([i=>i+5,i=>i*2,i=>i/5]);
```
###### İkinci aşama
Bu dönen işlevi çağırdığınız zaman girdileriniz ilk işleve iletilerek zincir başlatılır.

```javascript
 var Söz=Zincir(5);
 Söz.then(console.log) // 4
```

> Sırayla (5+5 =10, 10*2 =20, 20/5 =4) işlemleri yapılıp zincir tamamlandı, Söz çözümlendi.