JavaScript dilinde işlevler için bulunan **.bind** **.call** gibi yöntemlere ek olarak Otağ'da şu yöntemler vardır.

| Yöntem        | Açıklama |                           
| ------------- | --------------------------------------|
| [.debounce](İşlev.debounce)	| İşlevi gecikmeli işleve dönüştürür 	|
| [.prom](İşlev.prom)		| İşlevi Söze dönüştürür				|
