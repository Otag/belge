

| Yöntem        | Açıklama |                           
| ------------- | --------------------------------------|
| [[O.Page]]	| Sayfa omurgası/Geçmiş Yöneticisi.	|
| [[O.Chain]]	| İşlevleri sırayla işler.		|
| [[O.Time]]	| Zaman Nesnesi  			|
| [[O.i18n]]	| Uluslararasılaştırma. 		|
| [[O.Disk]]	| Yerel yığınak nesnesi.		|
| [[O.ready]]	| Belge yüklenme Sözü  			|
| [[O.define]]	| Model ya da bileşen tanımlar 		|
| [[O.combine]]	| Nesneleri birleştirir 		|
| [[O.resp]]	| Nesneye, duyarlı özellik tanımlar 	|
| [[O.req]]	| AJAX İstemcisi 	        	|
| [[O.toArray]]	| Dizi'ye dönüşüm 	        	|
