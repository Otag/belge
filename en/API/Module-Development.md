#Otağ Bileşenleri 

### 1-) Yapı / Çağrılma
Bileşenler bir [[Öge]]'nin belirli işleri yapabilmesi için özelleştirilmiş ve geliştirilmiş halidir. 

```javascript
'₺Bileşen'.init(Girdi,Girdi2);
```

```javascript
'₺Bileşen:Girdi,Girdi2'.init()
```

```javascript
'#Resimler'.extend('₺Bediz');
```

Bileşenler yukarıdaki biçimlerde çağrılabilmektedir.

Bu yüzden bir Bileşen this ile başlamalıdır .
```javascript
O.UI.Bileşen=function(girdiler){
    return this.init();
}
```

### 2-) Veri Arayüzü
Otağ'da [[Öge]]lerin [veri arayüzü](.has#bir-%C3%B6genin-veri-kar%C5%9F%C4%B1l%C4%B1%C4%9F%C4%B1--val-) vardır, Bileşenlerin de aynı şekilde veri arayüzü olmalıdır. 


```javascript
O.UI.Bileşen=function(girdiler){
    return this.has({
        a:'a',
        b:'input[type="text"]'
    });
}
var Öge='₺Bileşen'.init();
console.log(Öge.val);           //{a: null, b: null}
O.ready.then(b=>b.append(Öge)); //Sayfaya ekle
```

Bir value özelliği tanımlayarak öntanımlı arayüzün üstüne çıkabilirsiniz.

```javascript
O.UI.Bileşen=function(girdiler){
    return this.has({
        a:'a'.set('Adınız:'),
        b:'input[type="text"]'
    }).prop({
        value:function(){
            return this.V('b').value; 
        }
    });
}
var Öge='₺Bileşen'.init();
console.log(Öge.val);           // null
O.ready.then(b=>b.append(Öge)); //Sayfaya ekle
```
Yukarıdaki örnekte bir value yöntemi tanımlayarak Öge'nin arayüzünü yalnızca b adlı alt ögesinin değeri olacak şekilde ayarlamış olduk.
Ancak bu tek yönlü olduğu için yeterli değildir. Bileşenin değeri dışarıdan da değiştirilebilmelidir. Bunun için bir **.set** yöntemi tanımlanmalıdır. 
```javascript
O.UI.Bileşen=function(girdiler){
    return this.has({
        a:'a'.set('Adınız:'),
        b:'input[type="text"]'
    }).prop({
        value:function(){
            return this.V('b').value; 
        },
        set:function(value){
            this.V('b').value=value;
            return this; // ! Bunu yazmak önemli
        }
    });
}
var Öge='₺Bileşen'.init();
console.log(Öge.val);           // null
O.ready.then(b=>b.append(Öge)); //Sayfaya ekle
```