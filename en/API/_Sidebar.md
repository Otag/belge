|![](https://ilgilenio.github.io/Otag/img/o.min.svg) | Otağ™ JS |
|--|--|
| [Otağ](O)| [[Öge]] |
| [[Yazı]] | [[İşlev]] |
-----
[📅 Çizelge](https://ilgilenio.github.io/Otag/cizelge/)

[💬 Tartışma](https://t.me/OtagJS)

[👍🏻 Destekle](https://fb.me/OtagJS)