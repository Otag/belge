**Otag.ready.then(** _İşlev_ **)** => **Söz(**_document.body_**)**

Bir belge yüklenene kadar içine öge koyamayız. Bunun için **DOMContentLoaded** etkinliğinin çakılması gerekir.

```javascript
// Yalın JavaScript ile yazılmış
let Öge=document.createElement('div'); 		//Belge yüklenirken oluşturuldu
Öge.className='Öge';


document.addEventListener('DOMContentLoaded',function(){
	document.body.appendChild(Öge); 	//Belge yüklenince eklendi.
});
```

Bu yazışın uzun olması nedeniyle O.ready Sözü vardır. 
Belge yüklendiğinde Söz çözümlenir. 


```javascript
//Otağ.JS ile yazılmış
let Öge='Öge'.init();

O.ready.then(body=>body.append(Öge));
```

Çözümlemede ilk girdi olarak **document.body** verilir. Bu kod yazarken kolaylık sağlamak içindir.
