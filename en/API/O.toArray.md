**O.toArray(**  **)**
Dizi'ye benzer yapıları(_HTMLCollection_ vb.) ve Nesneleri Dizi'ye dönüştürmek için kullanılır.


```javascript
var Nesne={a:1,b:2};
O.toArray(Nesne); // [1,2]
```



### Sıralama
> _ adlı özellik tanımladığınız Nesneleri bu işlevden geçirmeyin.

Nesne Dizi'ye dönüşürken sıralama abeceye göredir. 
**_** özelliği tanımlayarak sıralamayı değiştirebilirsiniz. Bunu _Dizi_ ya da _Yazı_ biçiminde yapabilirsiniz.

```javascript
var Nesne={ç:'ç',a:1,d:'ğ',ı:'a',r:'a',_:['a','r','d','ı','ç']};  // <--- Dizi biçiminde
var Nesne={ç:'ç',a:1,d:'ğ',ı:'a',r:'a',_:'a,r,d,ı,ç'};            // <--- Yazı biçiminde
O.toArray(Nesne); // [1,'a','ğ','a','ç']
```
