[[.resp]] yöntemi gibidir. Tek farkı [[Öge]] dışındaki nesnelere de uygulanabilmesidir.

```javascript
    var Devingen={
        enerji:0,
        kütle:30,
        ivme:0,
        hız:0
    };
    O.resp.call(Devingen,{
        hız:function(hız){
            this.ivme=hız-this.hız;
            this.enerji=this.kütle*hız*hız/2;
        },
        
    });
```