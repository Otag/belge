**O.combine(** {},{}.. **)** => _Birleşik Nesne_ **{}**

Bu işlev **[].concat()** yönteminin nesneler için tasarlanmış olanıdır.
İstediğiniz kadar nesneyi girdi olarak vererek tek bir nesnede birleştirebilirsiniz.



### Kullanım
```javascript
let Birleşik=O.combine(
   {
       a:1,
       b:2
   },
   {
       c:3,
       d:4,
       e:5
   },
   {
      f:6,
      e:6    // öncekinin üstüne yazar
   }

);
console.log(Birleşik); // {a:1,b:2,c:3,d:4,e:6,f:6}
```

#### Uyarı
"_İstediğiniz kadar_" deyişi yorumlayıcıya göre değişiklik gösterebilir, çünkü girdi sayısı çok fazla olursa **RangeError** istisnası alabilirsiniz.

#### Ek/Uç bilgi
Otağ Çatısı, JavaScript dili uzantı(prototip) tabanlı olduğu için yöntem ağırlıklıdır.Buna rağmen **Nesne** yöntemi yerine ayrı bir işlev olarak tasarlanmasının sebebi Nesne uzantısının yapısından kaynaklı doğabilecek sıkıntıların önüne geçmektir.