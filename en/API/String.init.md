'#CSS.Seçici'**.init()**

**init** yöntemi Otağ'da **[[Öge]]** ve **[[Bileşen]]** oluşturma yöntemidir.

Otağ ile DOM [[Öge]]si oluşturmak için bir CSS metni yazın yeter.


```JavaScript
let b='b#öb.rek'.init()
```
Yazımı aşağıdaki yazıma denktir
```JavaScript
let b=document.createElement('b');
b.id='öb'
b.className='rek';
```

ve belge içinde şu şekilde bir Öge oluşturur

```html
<b id="öb" class="rek"></b>
```
div
-------
Genel bir etiket olduğu ve en çok kullanılan etiket olduğu için init() yönteminde  öntanımlı olan etiket div'dir.
Yani eğer bir etiket belirtmezseniz div Öge'si oluşturulur.
```JavaScript
let Öge1='Sınıf1 Sınıf2'.init(); 
let Öge2='div'.init().Class(['Sınıf1','Sınıf2']);
```
Yukarıdaki örnekte Öge1; Öge2'nin kolay/kısaltılmış yazımıdır.

Web Bileşenleri ve Keyfi Etiketler
-------
Otağ düz yazıyla anlatılmak istenilenin (Sınıf mı Etiket mi) belli olmadığı durumlarda iki şeye bakar
1. yazı tek sözcük mü yoksa birden fazla sözcük mü içeriyor?
2. tek sözcük ise büyük harfle mi başlamış
> Büyük harf/Birden fazla sözcükle anlatılmak istenenin sınıf olduğunu anlar
```JavaScript
var Öge='Dağ'.init();       //<div class="Dağ"></div>
var Öge='dağ'.init();       //<dağ></dağ>
var Öge='dağ bayır'.init(); //<div class="dağ bayır"></div>
```

Öge yöntemlerini doğrudan kullanma
-------
Yazı yazdıktan sonra [[Öge]] yöntemlerini direk kullanabilirsiniz, **.init** kendiliğinden çağrılır.

```javascript
let a='b#öb.rek'.init().disp()
let b='b#öb.rek'.disp()
```
Yani yukarıdaki a ile b yazımı aynı işi yapar.



Bileşen Çağırarak Oluşturma
-------
```JavaScript
'₺Bileşen.Sınıf#Kimlik'.init(arg1,arg2..)
```
Bu yazım **[[Bileşen]]**'i arg1,arg2.. argümanları ile oluşturur, Sınıf sınıfını ve Kimlik kimliğini atar.

```JavaScript
'₺Bileşen:arg1:arg2.Sınıf#Kimlik'.init()
```
Eğer Yazı türündeyse girdileri bu şekilde de verebilirsiniz. Böylece init yazmadan da diğer [[Öge]] yöntemlerini kullanabilirsiniz (Bkz:2.Örnek)

##### Somut olarak
[[Otağ.UI]].js'te bulunan [[UI.List]] Bileşenini Ağaç modeli ile (İlk girdi olarak "Ağaç" vererek)  oluşturalım

```JavaScript
let AğaçListesi='₺List:Ağaç.Ağaçlar'.init()
```

```html
<div class="Ağaçlar List"></div>
```

