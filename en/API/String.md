| Yöntem        | Açıklama |                           
| ------------- | --------------------------------------|
| [.vars](Yazı.vars)	| Değişkenli Yazıyı işler.		|
| [.from](Yazı.from)	| Nesneden Dizi oluşturur. 		|
| [.init](Yazı.init)	| Yeni [[Öge]] oluşturur. 	        	|
| [.get](Yazı.get)	| Sayfada bulunan [[Öge]]leri bulur.		|