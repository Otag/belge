## .debounce
Bu işlev prototipi işlev çağrılarını _sınırlandırmak_, _gecikmeli_ işlev tanımlamak amaçlarıyla kullanılır.
_Bağlam korunur._

**Örnek:**
```javascript
debounceLog=console.log.debounce(3000)
debounceLog('Armut') //3s sonra Armut yazar
```
Eğer bu örnekte 3s içinde/arka arkaya birden fazla debounceLog çağırırsanız sadece son çağrınız çalıştırılır.
- - - -
**Örnek 2**
```JavaScript
let Öge=''.disp(0) //Görünürlüğü Kapat
    .prop({
        disp:Element.prototype.disp.debounce(1000)
    })
O.ready.then(b=>b.append(Öge.disp(1))) //Görünürlüğü Aç,Sayfaya ekle
```
Yukarıdaki örnekte Otağ'da bulunan **[[.disp]]** yöntemini **.debounce** kullanılanıyla değiştirdik.
Öge belgeye konulduktan 1sn sonra gözükecek.


Ek Bilgi:

**.debounce** içinde bağlam korunduğu için bunu yapmamız olağan. Eğer korunmasaydı **[[.disp]]** uygulayacağımız öge, yöntem içinde ulaşılamazdı 
- - - -
**Örnek 3**
```JavaScript
document.addEventListener("scroll",(function(){
    console.log('çalıştı  ',+new Date)
}).debounce(100));
```
Bu örnek ise **.debounce** yönteminin en yaygın kullanıldığı örnektir. 
Çok fazla çağrılan işlevlerin belirli bir süre için sınırlandırılmasına olanak vererek yüksek iş gücü gerektiren işlevlerin sık çalışmasının önüne geçerek 
verimi artırır, taşınabilir(mobil) aygıtların pilini korur.