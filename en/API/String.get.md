'#CSS.Seçici'**.get()**

['#CSS.Seçici'.**init**](Yazı.init) yöntemi Otağ'da **[[Öge]]** ve **[[Bileşen]]** oluşturma yöntemidir.

'#CSS.Seçici'.**get** yöntemi ise Otağ'da belgedeki [[Öge]]leri getirmek içindir .

```css
a.bilgi{
	border:1px #09F solid;
}
```
biçimindeki bir CSS belgedeki _etiketi a, sınıfı bilgi_ olan bütün ögelere etki eder.

```javascript
'a.bilgi'.get()
```
biçimindeki bir Otağ yazımıyla aynı CSS'deki gibi belgede bulunan _etiketi a, sınıfı bilgi_ olan bütün ögeleri bulup üzerinde işlem yapabilirsiniz.
