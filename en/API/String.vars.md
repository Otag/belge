'Değişken₺ Barındıran Yazı' **.vars(** *Nesne{Değişken:'Yazı' }* **)**

**DeğişkenAdı₺** şeklinde tanımlanan değişken barındıran  [[Yazı]]'yı işler.

**₺,₸,₼,$** imleri değişken tanımlamada kullanılabilir.
 

```javascript
let Söz="Otağ'a hoşgeldin ad₺ soyad₸".vars({
    ad:'Kapgan'
    ,soyad:'Kağan'
});

```

Söz: Otağ'a hoşgeldin Kapgan Kağan