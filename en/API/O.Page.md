
**O.Page** : **Bet(_Sayfa_) Yöneticisi**

```javascript
let Betçi=O.Page();
```
### Yol tanımlama
Tanımlanan yol bir Öge, İşlev ya da başka bir yola yönlendirme olabilir. 
index ise ana bettir.

Aşağıdaki örnekte ana betten karşılama betine yönlendirme tanımladık.
```javascript
Betçi.routes.index='karşılama';       
Betçi.routes.karşılama='Öge'.init(); 

```
### Bet Taslağı

Bir Öge bet olduğunda **wake**, **idle**, **name** adlı özellikleri kullanışlı duruma gelir.

|||
|-|-|
|**wake**| Öge uyanınca çağrılacak işlev|
|**idle**| Öge uykuya geçerken çağrılacak işlev|
|**name**| Bet başlığı |

```javascript
...
Betçi.routes.atasözleri='Atasözleri Beti'.prop({
    name:'Atasözleri Beti',
    wake:function(altyol){
        return this;
    },
    idle:function(){
        return this;
    }
})
...
```
### Betler arası geçiş

.link yöntemi ile betten bete geçiş sağlanabilir.
```javascript
let Bağlantı = 'a.Bağlantı'.link('atasözleri');
```