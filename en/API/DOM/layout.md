**.layout(** şablon _Dizi_ **)**

İç içe çok fazla ekleme yapmak, düzenli şablon oluşturmak için kullanılır.
Değer olarak dizi alır.

```javascript
let öge='Öge'.layout([
    '#s1.sutun'.layout(['a','b'])
    ,'#s2.sutun'
]);
O.ready.then(b=>b.append(öge));
```

```html
<div class="Öge">
     <div class="sutun" id="s1">
         <a></a>
         <b></b>
     </div>
     <div class="sutun" id=“s2"></div>
</div>
```

Buraya kadar **[[.append]]** yöntemi ile farkı yoktu
ancak **.layout** yönteminin bir görevi bunu daha kısa biçimde yazmak.

Şablon dizisinde öge olarak dizi verilerek öz yineleme sağlanmış olur.
Aşağıdaki örnek yukarıdaki örneğin kısa yazımıdır.

```javascript
'Öge'.layout([
    ['#s1.sutun',['a','b']]
    ,'#s2.sutun'
]);
```

Bu şekilde iç içe istediğiniz kadar yazabilirsiniz

Burada şablon yönteminin yaptığı güzel bir iş ise **[[.has]]** ile eklenen **View** bağlarını kullandırabilmesidir.

```javascript
let Baş='Baş'.has({
    gövde:'Gövde'.has({
        ayak:'Ayak'
    })
}).layout([
    ['#s1.sutun',['a','b']]
    ,['#s2.sutun',['gövde','gövde:ayak']]
]);
O.ready.then(b=>b.append(Baş));
```

```html
<div class="Baş">
     <div class="sutun" id="s1">
         <a></a>
         <b></b>
     </div>
     <div class="sutun" id="s2">
           <div class="Gövde">
               <div class="Ayak"></div>
           </div>
     </div>
</div>
```