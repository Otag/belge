**.destroy(** başlangıç, yoketmeSüresi **)**

| Girdi             | Tür    | Birim | Öntanımlı | Açıklama |
|-------------------|--------|-------|-----------|----------|
| **başlangıç**     | _Sayı_ | ms    | 0         | Sınıfın ekleneceği zaman |
| **yoketmeSüresi** | _Sayı_ | ms    | 300       | Eklendikten ne kadar sonra Öge'nin kaldırılacağı|


[[Öge]]'ye çağrıldıktan **başlangıç** kadar zaman sonra **_destroy_** sınıfı ekler ve **yoketmeSüresi** sonunda [[Öge]]'yi sayfadan kaldırır. 

Bir CSS ile birlikte kullanılması durumunda mantıklıdır.

```css
.destroy {
    opacity: 0;
    transition: .3s cubic-bezier(0.9, -0.5, 1, 1);
    transform: scale(.0);
}
```
**yoketmeSüresi**nin değeri CSS ile tanımlanan **transition-duration** özelliğinde belirtilen zaman kadar olması önerilir.

Bu yöntem [[Öge]] kaldırıldığında çözümlenecek bir Söz döndürür.
