**.append(** ögeler _Dizi_,  ters _Mantıksal_ **)**

Bir ögenin içine öge/ler ekler.

_Eklenen öğeler ile ata öge arasında bağ yoktur(.parent/.View bağı) Bağlı ekleme için **[[.has]]** yöntemini kullanınız._


```javascript
let Öge='Ata'.append([
    'Çocuk'.init(),
    'Çocuk2'.init()
]);
O.ready.then(b=>b.append(Öge))
```


```html
<div class="Ata">
    <div class="Çocuk"></div>
    <div class="Çocuk2"></div>
</div>
```

Eğer tersten eklemek isterseniz. 2. girdiyi "Mantıksal Doğru" verin


```javascript
let Öge='Ata'.append([
    'Çocuk'.init(),
    'Çocuk2'.init()
],true);
O.ready.then(b=>b.append(Öge))
```


```html
<div class="Ata">
    <div class="Çocuk2"></div>
    <div class="Çocuk"></div>
</div>
```