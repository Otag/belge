**.Class(** Sınıflar _Dizi|Yazı_, Kaldır _Mantıksal_ **)**

Bir Öge'ye sınıf/lar ekler ya da kaldırır.

### Kaldırma
```javascript
var Öge='.bu.bir.Öge'.Class('bir',1); // 'bir' sınıfını kaldır;

O.ready.then(b=>b.append(Öge));
```

```html
<div class="bu Öge"></div>
```
### Ekleme
```javascript
Öge.Class(['çok','yeni']);
```

```html
<div class="bu Öge çok yeni"></div>
```