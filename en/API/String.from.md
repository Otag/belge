'Virgülle,Ayrılmış,Yazı'**.from(** *Nesne{ }* **)**

**.from** bir **[[Yazı]]** yöntemidir.
İstenilen anahtarlara ait değerleri Nesne içinden bularak bir Dizi oluşturur. Girilen sıra önemlidir.

```javascript
'gün,ay,yıl'.from({
    gün:30,
    yıl:1922,
    ay:8
});
//Dizi [30,8,1922]
```

JavaScript dilinde **[[Öge]]** de bir Nesne olduğu için [[Öge]]nin özelliklerini de getirebilirsiniz

```javascript
Öge='.Bir#Öge'.init();

'id,className'.from(Öge);
//Dizi ['Öge','Bir']
```