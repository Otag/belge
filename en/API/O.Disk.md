## Otag.Disk

Herhangi bir veriyi aygıtın yerel veri yığınağında tutabilmek ve tutulan bu verilere geri erişebilmek için tasarlanmıştır.
Veri JSON biçimine dönüştürülerek depolanır.

**O.Disk**' veriyi RAM (Denkgele Erişim Belleği) yerine Yerel Yığınakta (istemci tarayıcısındaki bellekte) tutmaya yarar.


### Kullanım

```javascript
	O.Disk.Oğuzlar={
		Bozoklar:{
			Günhan:['Kayı','Bayat','Alkaevli','Karaevli']
			,Ayhan:['Yazır','Döğer','Dodurga','Yaparlı']
			,Yıldızhan:['Avşar','Kızık','Beğdili','Karkın']
		}
		,Üçoklar:{
			Gökhan:['Bayındır','Peçenek','Çavuldur','Çepni']
			,Dağhan:['Salur','Eymür','Alayuntlu','Yüregir']
			,Denizhan:['İğdir','Büğdüz','Yıva','Kınık']
		}
	}	//Yerel yığınağa Oğuzlar'ı yaz

	O.Disk.Oğuzlar.Bozoklar.Ayhan // Yerel Yığınaktan Oğuzlar'ı oku
	// ['Yazır','Döğer','Dodurga','Yaparlı']
```


```javascript
	O.Disk[1]='Baş';
	O.Disk[2]='Taş';
	O.Disk['Toprak']='Toprak';
	
	delete O.Disk[2]; 		//Tekli silme
	O.Disk.rem([1,'Toprak']);	//Çoklu silme
```

### Uyarı
O.Disk ile yazacağınız veriler, yerel yığınakta tutulduğu için belge ya da tarayıcı kapatıldığında yok olmaz.
Genel kullanım niteliğini anlayabilmek için bilmiyorsanız **Window.localStorage** kavramına bakmanız önerilir.
