.toArray( _Object_ )

Converts **Object** or **arguments** to **Array**

``` javascript
   let arr = Nesne.toArray({a: 1,b: 2,c: 3})
   
   console.log(arr) // [1, 2, 3]
```