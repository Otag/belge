.cumul( _Object_, _Object_, ...  )

Combines Objects by summing values.

``` javascript
let obj  = {a: 8, b: 2, c: 3, d: -2}
,  obj2  = {c: 4, d: 1, e: 2}
,  final = Nesne.cumul(obj, obj2)
    
console.log(final) // {a: 8, b: 2, c: 7, d: -1, e :2} // no value loss
```