.map( **obj** _Object_, **map** _Object_, **limit** _Boolean_, **default** )

Creates an Object with key mapping

``` javascript
    let obj   = {a: 5, b: 6, c: 16, d: 18}
    ,   map   = {a: 'b', c: 'a', b: 'c'}
    ,   final = Nesne.map(obj, map)
        
    console.log(final) // {a: 6, b: 16, c: 5, d: 18}
```

##### Limit obtained keys

You can also limit obtained keys which is not specified in map argument

``` javascript
    let obj   = {a: 5, b: 6, c: 16, d: 18}
    ,   map   = {a: 'b',c: 'a', b: 'c'}
    ,   final = Nesne.map(obj, map, true)
        
    console.log(final) // {a: 6, b: 16, c: 5} // d is not obtained
```

##### Use default value

You can use default value for absent values

``` javascript
    let obj   = {a: 5, b: 6, c: 16, d: 18}
    ,   map   = {a: 'b',c: 'a', b: 'c', e: 'f'}
    ,   final = Nesne.map(obj, map, true, 25)
        
    console.log(final) // {a: 6, b: 16, c: 5, e:25} 
    // obj.f isn't exist, so default value is given to e
```

