.match( **obj** _Object_, **obj2** _Object_, **fields** _Array_, **default** )

Transfers values of given fields from obj2 to obj, if value not exists, uses default value

``` javascript
    let obj   = {}
    ,   obj2  = {a: 5, c: 6, d: 16}
    ,   final = Nesne.match(obj, obj2, ['a', 'd', 'e'], 12)
    
    console.log(final) // {a: 5, d: 16, e :2} // e is default
```

##### Another default use case

define default values as an Object, use **_def** to define default value for all not existing keys

``` javascript
    let obj   = {}
    ,   obj2  = {a: 5, c: 6, d: 16}
    ,   final = Nesne.match(
            obj, obj2,
            ['a', 'd', 'e', 'f', 'g', 'h'], {e: 12, _def:10})
    
    console.log(final) // {a: 5, d: 16, e :2, f: 10, g: 10, h:10}
```