## Installation

```bash
    npm i nesne -g
```

```javascript   
    import Nesne from 'nesne'
```

#### Traditional use

```html
    <script src="https://cdn.jsdelivr.net/npm/nesne@1.0.1/Nesne.min.js"></script>
```
