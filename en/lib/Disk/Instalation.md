## Installation

```bash
    npm i o.disk -g
```

#### Use as ES6 Module

```javascript   
    import Disk from 'o.disk'
```

#### Use as NodeJs Module

```javascript   
    const Disk = require('o.disk')
```

> Writes data to .odisk/ folder when you use O.Disk as a node module

#### Traditional Use

Add to your HTML document

```html
<script src="https://cdn.jsdelivr.net/npm/o.disk@2.0.3/index.min.js"></script>
```
