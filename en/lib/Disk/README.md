# O.Disk

O.Disk is an abstraction over (**localStorage**). You can save data directly to disk with clean simple interface.


```javascript
O.Disk.açar={
    a: 1
}
// localStorage.setItem(JSON.stringify({a: 1}))
```


You can use **O.Disk** as **ES6 Module** at front-end side.

You can also use as **Node Module** at back-end side. It saves data to .odisk/ folder in apps workspace.

### License

MIT © OtagJS.org 
