
API is same and simple at both frontend and backend side.


## Write to disk
```javascript
Disk.key = 'value'
```

### Write an Object to disk
```javascript
Disk.obj = {
    key: 'value'
}
```

## Read from disk
```javascript
console.log(Disk.key) // value

console.log(Disk.obj.key) // value
```

## Delete from disk
```javascript
delete Disk.key
console.log(Disk.key) // null
```

#### Batch delete from disk
```javascript
Disk.rem(['key', 'obj'])
```

#### Expire key
```javascript
Disk.expire('key', 20) // expire after 20s
```