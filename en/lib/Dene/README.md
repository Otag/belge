
# Dene

<img src="https://media.giphy.com/media/cdI9qrqmXPTtLKu5bQ/giphy.gif" width="300px">

Dene is a unit testing framework to test library methods etc.

## Installation

```bash
npm i dene --save-dev
```

![](https://nodei.co/npm/dene.png)

## License

MIT © OtagJS.org 