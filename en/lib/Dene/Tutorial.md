## Tutorial

Write test cases


```javascript
// dene.js

let Dene=require('dene')

let Tests={
  simpleTest: () => return true,
  asyncTest:  () => {
    return new Promise((resolve,reject) => {

      setTimeout(resolve,1500) // Succeed after 1.5s

    }
  }
}

Dene(Tests)
```

Add your test script to package.json

```json
  "scripts":{
    "test":"node dene.js"
  }

```

Run tests at terminal

```bash
npm test
```
